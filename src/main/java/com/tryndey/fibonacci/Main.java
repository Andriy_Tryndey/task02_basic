package com.tryndey.fibonacci;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int start = 0;
        int end = 0;
        int sumOdd = 0;
        int sumEven = 0;
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.println("Input start number");
        start = sc.nextInt();
        System.out.println("Input end number");
        end = sc.nextInt();
        System.out.println("odd numbers from start to the end");
        for (int i = start; i <= end; i++) {
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }
        System.out.println("odd numbers from end to the start");
        for (int j = end; j >= start; j--) {
            if (j % 2 == 1) {
                System.out.println(j);
            }
        }
        System.out.println("sum of odd and even numbers");
        for (int i = start; i <= end; i++) {
            if (i % 2 == 0) {
                sumOdd = sumOdd + i;
            } else {
                sumEven = sumEven + i;
            }
        }
        System.out.println("sumOdd = " + sumOdd);
        System.out.println("sumEven = " + sumEven);

        System.out.println("Fibonacci numbers");
        int a = 1;
        int b = 1;
        int c;
        int i = 3;
        System.out.println("Enter the size of set");
        n = sc.nextInt();
        int[] mas = new int[n];
        if (n == 1) {
            mas[0] = 0;
            System.out.print(0 + ",");
        }
        if (n == 2) {
            mas[0] = 0;
            mas[1] = 1;
            System.out.print(0 + ", " + 1 + ",");
        }
        if (n == 3) {
            mas[0] = 0;
            mas[1] = 1;
            mas[2] = 1;
            System.out.print(0 + ", " + 1 + ", " + 1);
        }
        if (n > 3) {
            System.out.print(0 + ", " + 1 + ", " + 1 + ", ");
            while (i < n) {
                c = a + b;
                mas[0] = 0;
                mas[1] = 1;
                mas[2] = 1;
                System.out.print(c + ", ");
                mas[i] = c;
                a = b;
                b = c;
                i++;
            }
            System.out.println("");
        }
        int f1 = 0;
        int f2 = 0;
        int pOdd;
        int peven;
        int count = 0;
        System.out.println("Odd number");
        for (int j = 0; j < mas.length; j++) {
            if (mas[j] % 2 == 1) {
                System.out.print(mas[j] + ", ");
                if (mas[j] > f1) {
                    f1 = mas[j];
                }
                count++;
            }
        }
        System.out.println("");
        pOdd = count * 100 / mas.length;
        System.out.println("Percentage of odd = " + pOdd + "%");

        System.out.println("The biggest odd number = " + f1);
        System.out.println("Even number");
        count = 0;
        for (int k = 0; k < mas.length; k++) {
            if (mas[k] % 2 == 0) {
                System.out.print(mas[k] + ", ");
                if (mas[k] > f2) {
                    f2 = mas[k];
                }
                count++;
            }
        }
        System.out.println("");
        peven = count * 100 / mas.length;
        System.out.println("Percentage of even = " + peven);
        System.out.println("The biggest even namber = " + f2);


    }
}
